# Ubuntu 20.04 LTS (Focal Fossa) Ansible Image

> _NOTE: The files in this folder are forked from https://github.com/geerlingguy/docker-ubuntu2004-ansible_

Ubuntu 20.04 LTS (Focal Fossa) Docker container for Ansible playbook and role testing.


## Tags

The available [tags](https://gitlab.com/goldflam-gmbh/tools/docker-ubuntu2004-ansible/container_registry/1659573) represent different Ansible versions and are maintained manually.


## How to Build

This image is built on Docker Hub automatically any time the upstream OS container is rebuilt, and any time a commit is made or merged to the `main` branch. But if you need to build the image on your own locally, do the following:

  1. [Install Docker](https://docs.docker.com/install/).
  2. `cd` into this directory.
  3. Run `docker build --build-arg ansible_version=<ansible-version> -t registry.gitlab.com/goldflam-gmbh/tools/docker-ubuntu2004-ansible:<ansible-version> .`
  4. Push to GitLab.com project registry:
     * `docker login registry.gitlab.com`
     * `docker push registry.gitlab.com/goldflam-gmbh/tools/docker-ubuntu2004-ansible:<ansible-version>`


## How to Use

  1. [Install Docker](https://docs.docker.com/engine/installation/).
  2. Pull this image from Docker Hub:
     * `docker login registry.gitlab.com`
     * `docker pull registry.gitlab.com/goldflam-gmbh/tools/docker-ubuntu2004-ansible:<ansible-version>` (or use the image you built earlier).
  3. Run a detached container from the image: `export DOCKER_ID_ANSIBLE=$(docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/goldflam-gmbh/tools/docker-ubuntu2004-ansible:<ansible-version>)`
     * To test your project mount a volume from the current project working directory with `--volume=$(pwd):/data:rw`).
  4. Use Ansible inside the container:
    a. `docker exec --tty $DOCKER_ID_ANSIBLE env TERM=xterm ansible --version`
    b. `docker exec --tty $DOCKER_ID_ANSIBLE env TERM=xterm ansible-playbook /path/to/ansible/playbook.yml --syntax-check`


## Author

Created in 2020 by [Jeff Geerling](https://www.jeffgeerling.com/), author of [Ansible for DevOps](https://www.ansiblefordevops.com/).

Forked and changed in 2021 by Tobias Fischer, Goldflam GmbH.